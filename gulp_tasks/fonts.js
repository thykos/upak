import gulp from 'gulp';
import newer from 'gulp-newer';
import {Fonts, PublicFonts, BowerDir} from './dist';

gulp.task('copy:fonts', function () {
  return gulp.src(
    [Fonts,
    `${BowerDir}/font-awesome/fonts/*.*`,
    `${BowerDir}/bootstrap/fonts/*.*`
    ])
    .pipe(newer(Fonts))
    .pipe(gulp.dest(PublicFonts))
});

export default gulp.task('fonts', ['copy:fonts'], () =>
  gulp.watch(Fonts, ['copy:fonts'])
);
