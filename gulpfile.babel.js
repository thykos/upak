import gulp from 'gulp';
import server from './gulp_tasks/server';
import clean from './gulp_tasks/clean';
import pug from './gulp_tasks/pug';
import scripts from './gulp_tasks/scripts';
import styles from './gulp_tasks/styles';
import images from './gulp_tasks/images';
import fonts from './gulp_tasks/fonts';
import {addDepScripts, addDepStyles} from './gulp_tasks/dependencies';

gulp.task('start', ['pug', 'scripts', 'styles', 'images', 'fonts', 'addDepScripts', 'addDepStyles', 'server']);
